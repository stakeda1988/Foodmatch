//
//  BadCustomCell.swift
//  Foodmatch
//
//  Created by SHOKI TAKEDA on 11/7/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class BadCustomCell: UITableViewCell {
    @IBOutlet weak var BadName: UILabel!
    @IBOutlet weak var BadIconImage: UIImageView!

    @IBOutlet weak var BadRightIconImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(bood :Bood) {
        self.BadName.text = bood.name as String
        let imageData:String = bood.imageUrl! as String
        self.BadIconImage.image = UIImage(named:imageData)
        let rightImageData:String = bood.rightImageUrl! as String
        self.BadRightIconImage.image = UIImage(named:rightImageData)
    }
}
