//
//  ResultViewController.swift
//  Foodmatch
//
//  Created by SHOKI TAKEDA on 11/2/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class ResultViewController: UIViewController, NADViewDelegate{
    private var nadView: NADView!
    
    @IBAction func badBackBtn(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    @IBAction func backBtn(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        // NADViewクラスを生成
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        // 広告枠のapikey/spotidを設定(必須)
        nadView.setNendID("eb5ca11fa8e46315c2df1b8e283149049e8d235e",
            spotID: "70996")
        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        
        // 通知有無にかかわらずViewに乗せる場合
        self.view.bringSubviewToFront(nadView)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit{
        
        // delegateには必ずnilセットして解放する
        nadView.delegate = nil
        nadView = nil
        
    }
}
