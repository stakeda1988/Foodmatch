//
//  Food.swift
//  Foodmatch
//
//  Created by SHOKI TAKEDA on 11/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import Foundation

class Food : NSObject {
    var name:NSString
    var imageUrl:NSString?
    var rightImageUrl:NSString?
    
    init(name: String, imageUrl: NSString?, rightImageUrl:NSString?){
        self.name = name
        self.imageUrl = imageUrl
        self.rightImageUrl = rightImageUrl
    }
}