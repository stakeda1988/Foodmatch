//
//  SecondViewController.swift
//  Foodmatch
//
//  Created by SHOKI TAKEDA on 11/2/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,NADViewDelegate {
    
    private var nadView: NADView!
    
    @IBOutlet weak var tableView: UITableView!

    var boods:[Bood] = [Bood]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        
        // NADViewクラスを生成
        nadView = NADView(frame: CGRect(x: myBoundSize.width/2-160, y: myBoundSize.height-100, width: 320, height: 50))
        // 広告枠のapikey/spotidを設定(必須)
        nadView.setNendID("6011089db7691d02c3c9c4e6092c7be61b8714a2",
            spotID: "475395")
        // nendSDKログ出力の設定(任意)
        nadView.isOutputLog = false
        // delegateを受けるオブジェクトを指定(必須)
        nadView.delegate = self
        // 読み込み開始(必須)
        nadView.load()
        
        // 通知有無にかかわらずViewに乗せる場合
        self.view.addSubview(nadView)
        
        self.navigationController?.navigationBar.barTintColor = UIColor(red: 246/255, green: 241/255, blue: 227/255, alpha: 0.5)
        self.setupFriends()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //        tableView.backgroundView = nil
        //        tableView.backgroundColor = UIColor.clearColor()
        //        tableView.separatorColor = UIColor.clearColor()
        //        let myBoundSize: CGSize = UIScreen.mainScreen().bounds.size
        //        let imageView = UIImageView(frame: CGRectMake(0, 0, myBoundSize.width, myBoundSize.height))
        //        let image = UIImage(named: "01.jpg")
        //        imageView.image = image
        //        imageView.alpha = 1.0
        //        tableView.backgroundView = imageView
        tableView.backgroundView = nil
        tableView.backgroundColor = UIColor.clearColor()
        tableView.separatorColor = UIColor.clearColor()
        let imageView = UIImageView(frame: CGRectMake(0, 0, myBoundSize.width, myBoundSize.height))
        let image = UIImage(named: "02.jpg")
        imageView.image = image
        imageView.alpha = 1.0
        tableView.backgroundView = imageView
    }
    
    func setupFriends() {
        let f1 = Bood(name: "Ken", imageUrl: "01.jpg", rightImageUrl: "01.jpg")
        let f2 = Bood(name: "Erika", imageUrl: "02.jpg", rightImageUrl: "02.jpg")
        let f3 = Bood(name: "Anna", imageUrl: "03.jpg", rightImageUrl: "03.jpg")
        
        boods.append(f1)
        boods.append(f2)
        boods.append(f3)
    }
    
    //    let texts = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    // セルの行数
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return boods.count
    }
    
    // セルの内容を変更
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell: BadCustomCell = tableView.dequeueReusableCellWithIdentifier("BadCustomCell", forIndexPath: indexPath) as! BadCustomCell
        
        cell.setCell(boods[indexPath.row])
        
        //        cell.textLabel?.text = texts[indexPath.row]
        cell.backgroundColor = UIColor.clearColor()
        let cellSelectedBgView = UIView()
        cellSelectedBgView.backgroundColor = UIColor.clearColor()
        cell.selectedBackgroundView = cellSelectedBgView
        cell.contentView.backgroundColor = UIColor.clearColor()
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.textColor = UIColor.whiteColor()
        return cell
    }
    
    func tableView(table: UITableView, didSelectRowAtIndexPath indexPath:NSIndexPath) {
        performSegueWithIdentifier("shift",sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject!) {
        if segue.identifier == "shift" {
            let taskController:UINavigationController
            = segue.destinationViewController as! UINavigationController
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
