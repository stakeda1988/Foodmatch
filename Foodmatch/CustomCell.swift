//
//  CustomCell.swift
//  Foodmatch
//
//  Created by SHOKI TAKEDA on 11/5/15.
//  Copyright © 2015 handsomeslot.com. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var rightIconImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setCell(food :Food) {
        self.name.text = food.name as String
        let imageData:String = food.imageUrl! as String
        self.iconImage.image = UIImage(named:imageData)
        let rightImageData:String = food.rightImageUrl! as String
        self.rightIconImage.image = UIImage(named:rightImageData)
    }
}
